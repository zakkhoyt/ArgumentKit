//
//  ArgumentManagerTests.swift
//  ArgumentManagerTests
//
//  Created by Zakk Hoyt on 4/19/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import XCTest

@testable import ArgumentKit

class ArgumentManagerTests: XCTestCase {
    enum Argument: ArgumentDefinable, CaseIterable {
        case alertErrors
        case darkMode
        case beVerbose
        
        var title: String {
            switch self {
            case .alertErrors: return "XCT_ALERT_ERRORS"
            case .darkMode: return "XCT_DARK_MODE"
            case .beVerbose: return "XCT_BE_VERBOSE"
            }
        }

        var description: String {
            switch self {
            case .alertErrors: return "Present error in an UIAlertController."
            case .darkMode: return "Render the app using an alternative color set."
            case .beVerbose: return "When printing text or descriptions, be very very very verbose, including this. When printing text or descriptions, be very very very verbose, including this. When printing text or descriptions, be very very very verbose, including this. When printing text or descriptions, be very very very verbose, including this."
            }
        }
    }
    
    
    
    var argumentManager: ArgumentManager!
    override func setUp() {
        super.setUp()
        argumentManager = ArgumentManager()
        argumentManager.arguments = Argument.allCases

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExampleConfig() {
        
        
        ArgumentManagerUserDefaults.printArguments()
        
        // Disable all args and test API
        disableAll()
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.beVerbose))
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.darkMode))
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.alertErrors))
        
        // Enable all args in the userDefault category and test API
        enableUserDefaults()
        XCTAssertTrue(argumentManager.argumentEnabled(Argument.beVerbose))
        XCTAssertTrue(argumentManager.argumentEnabled(Argument.darkMode))
        XCTAssertTrue(argumentManager.argumentEnabled(Argument.alertErrors))
        
        // Disable all args and test API
        disableAll()
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.beVerbose))
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.darkMode))
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.alertErrors))
        
        // Enable all args in the runtime category and test API
        enableRunTime()
        XCTAssertTrue(argumentManager.argumentEnabled(Argument.beVerbose))
        XCTAssertTrue(argumentManager.argumentEnabled(Argument.darkMode))
        XCTAssertTrue(argumentManager.argumentEnabled(Argument.alertErrors))
        
        // Disable all args and test API
        disableAll()
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.beVerbose))
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.darkMode))
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.alertErrors))
        
        
        // Enable all args in the userDefault category and test API
        enableUserDefaults()
        XCTAssertTrue(argumentManager.argumentsEnabled([Argument.beVerbose, Argument.darkMode, Argument.alertErrors]))
        
        // Disable all args and test API
        disableAll()
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.beVerbose))
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.darkMode))
        XCTAssertFalse(argumentManager.argumentEnabled(Argument.alertErrors))
        
        // Enable all args in the runtime category and test API
        enableRunTime()
        XCTAssertTrue(argumentManager.argumentsEnabled([Argument.beVerbose, Argument.darkMode, Argument.alertErrors]))
        
        
    }
    
    
    private func disableAll() {
        for argument in argumentManager.arguments {
            argumentManager.modifyRuntimeArgument(argument, enable: false)
            argumentManager.modifyUserDefaultsArgument(argument, enable: false)
        }
    }
    
    private func enableUserDefaults() {
        for argument in argumentManager.arguments {
            argumentManager.modifyUserDefaultsArgument(argument, enable: true)
        }
    }
    
    private func enableRunTime() {
        for argument in argumentManager.arguments {
            argumentManager.modifyRuntimeArgument(argument, enable: true)
        }
    }
    
    
    
}
