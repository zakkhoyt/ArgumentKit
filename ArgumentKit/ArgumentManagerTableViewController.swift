//
//  ArgumentManagerTableViewController.swift
//  ArgumentManagerExample
//
//  Created by Zakk Hoyt on 4/18/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import UIKit

extension ArgumentManagerTableViewController {
    /// Returns an instance for presenting modally which includes a close button and navBar styled type header
    static public func modalInstance() -> ArgumentManagerTableViewController {
        let vc = ArgumentManagerTableViewController(nibName: nil, bundle: nil)
        vc.addNavigationBar()
        return vc
    }
    
    /// Returns an instance suitable for presenting in a UINavigationController or UITabController
    static public func instance() -> ArgumentManagerTableViewController {
        let vc = ArgumentManagerTableViewController(nibName: nil, bundle: nil)
        return vc
    }
}

private enum TableSection: Int {
    case arguments
    case legend
    case about
    
    static let count = 3
    
    var title: String? {
        switch self {
        case .legend:
            return "Legend"
        case .arguments:
            return "Arguments"
        case .about:
            return "About"
        }
    }
}

private enum LegendRow: Int {
    case xcode
    case userDefaults
    case runtime
    
    static let count = 3

    var title: String {
        switch self {
        case .xcode:
            return "Xcode"
        case .userDefaults:
            return "UserDefaults"
        case .runtime:
            return "Runtime"
        }
    }

    var about: String {
        switch self {
        case .xcode:
            return "Argument is enabled/disabled by Xcode by adding an argument under Scheme ▻ Run ▻ Arguments. These arguments are only active if Xcode's debugger launched the app. App has read-only permissions."
        case .userDefaults:
            return "Argument is enabled/disabled by the user and is stored in UserDefaults. Sticky across app sessions (non-volatile). Arguments continue to function outside of the debug environment. App has read-write permissions."
        case .runtime:
            return "Argument is enabled/disabled by the user and is stored in RAM. State is cleared every app session (volatile). Arguments continue to function outside of the debug environment. App has read-write permissions."
        }
    }
    var detail: String {
        switch self {
        case .xcode:
            return "App has read-only permissions"
        case .userDefaults:
            return "App has read/write permissions. Tap appropriate button to toggle"
        case .runtime:
            return "App has read/write permissions. Tap appropriate button to toggle"
        }
    }

    var disabledImage: UIImage {
        switch self {
        case .xcode:
            return UIImage(named: "xcode_30x30", in: bundle, compatibleWith: nil)!
        case .userDefaults:
            return UIImage(named: "userDefaults_30x30", in: bundle, compatibleWith: nil)!
        case .runtime:
            return UIImage(named: "runtime_30x30", in: bundle, compatibleWith: nil)!
        }
    }

    var enabledImage: UIImage {
        switch self {
        case .xcode:
            return UIImage(named: "xcode_filled_30x30", in: bundle, compatibleWith: nil)!
        case .userDefaults:
            return UIImage(named: "userDefaults_filled_30x30", in: bundle, compatibleWith: nil)!
        case .runtime:
            return UIImage(named: "runtime_filled_30x30", in: bundle, compatibleWith: nil)!
        }
    }

}

private var bundle: Bundle {
    return Bundle(for: ArgumentManagerTableViewController.self)
}

public class ArgumentManagerTableViewController: UITableViewController {
    private var argumentManager: ArgumentManager = ArgumentManager.shared
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Arguments"
        setupTableView()
    }

    /// Adds a tableHeaderView which resembles as UINavigationBar. Includes a close button.
    internal func addNavigationBar() {
        guard let tableHeaderView = bundle.loadNibNamed(ArgumentManagerTableHeaderView.identifier, owner: self, options: [:])?.first as? ArgumentManagerTableHeaderView else {
            assert(false, "")
            return
        }
        tableHeaderView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 44)
        tableHeaderView.closeButtonTapped = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        tableView.tableHeaderView = tableHeaderView
    }

    private func setupTableView() {
        func setupCells() {
            do {
                let nib = UINib(nibName: ArgumentTableViewCell.identifier, bundle: bundle)
                tableView.register(nib, forCellReuseIdentifier: ArgumentTableViewCell.identifier)
            }
            do {
                let nib = UINib(nibName: InstructionTableViewCell.identifier, bundle: bundle)
                tableView.register(nib, forCellReuseIdentifier: InstructionTableViewCell.identifier)
            }
            do {
                let nib = UINib(nibName: AboutTableViewCell.identifier, bundle: bundle)
                tableView.register(nib, forCellReuseIdentifier: AboutTableViewCell.identifier)
            }

        }
        setupCells()
        
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100.0
    }


    // MARK: Impelements UITableViewDataSource

    override public func numberOfSections(in tableView: UITableView) -> Int {
        return TableSection.count
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let tableSection = TableSection(rawValue: section)!
        switch tableSection {
        case .legend:
            return LegendRow.count
        case .arguments:
            return argumentManager.arguments.count
        case .about:
            return 1
        }
    }

    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableSection = TableSection(rawValue: indexPath.section)!
        switch tableSection {
        case .legend:
            let cell = tableView.dequeueReusableCell(withIdentifier: InstructionTableViewCell.identifier, for: indexPath) as! InstructionTableViewCell
            let legend = LegendRow(rawValue: indexPath.row)!
            cell.legendTitleLabel.text = legend.title
            cell.legendAboutLabel.text = legend.about
            cell.disabledImageView.image = legend.disabledImage
            cell.enabledImageView.image = legend.enabledImage
            return cell
        case .arguments:
            let cell = tableView.dequeueReusableCell(withIdentifier: ArgumentTableViewCell.identifier, for: indexPath) as! ArgumentTableViewCell
            let argumentPresenceItem = argumentManager.argumentPresenceItems[indexPath.row]
            cell.argumentPresenceItem = argumentPresenceItem
            cell.xcodeButtonTapped = {
                // Xcode args cannot be changed at runtime
            }
            cell.userDefaultsButtonTapped = { [weak self] in
                if argumentPresenceItem.presence.contains(.userDefaults) {
                    self?.argumentManager.modifyUserDefaultsArgument(argumentPresenceItem, enable: false)
                } else {
                    self?.argumentManager.modifyUserDefaultsArgument(argumentPresenceItem, enable: true)
                }
                
                self?.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            cell.runtimeButtonTapped = { [weak self] in
                if argumentPresenceItem.presence.contains(.runtime) {
                    self?.argumentManager.modifyRuntimeArgument(argumentPresenceItem, enable: false)
                } else {
                    self?.argumentManager.modifyRuntimeArgument(argumentPresenceItem, enable: true)
                }
                self?.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            return cell
        case .about:
            let cell = tableView.dequeueReusableCell(withIdentifier: AboutTableViewCell.identifier, for: indexPath) as! AboutTableViewCell
            return cell
        }
    }
    
    public override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let tableSection = TableSection(rawValue: section)!
        return tableSection.title
    }
}


