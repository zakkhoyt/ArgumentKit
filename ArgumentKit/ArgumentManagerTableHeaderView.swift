//
//  ArgumentManagerTableHeaderView.swift
//  ArgumentManager
//
//  Created by Zakk Hoyt on 4/22/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import UIKit



typealias ArgumentManagerTableHeaderViewButtonTapped = () -> Void

class ArgumentManagerTableHeaderView: UIView {
    static let identifier = "ArgumentManagerTableHeaderView"
    
    var closeButtonTapped: ArgumentManagerTableHeaderViewButtonTapped?

    override func awakeFromNib() {
        super.awakeFromNib()
        //F9F9F9 is default nav bar color
        let f: CGFloat = 0xF9 / 0xFF
        self.backgroundColor = UIColor(red: f, green: f, blue: f, alpha: 1.0)
    }

    @IBAction func closeButtonTouchUpInside(_ sender: Any) {
        closeButtonTapped?()
    }
}
