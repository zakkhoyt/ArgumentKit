//
//  InstructionTableViewCell.swift
//  ArgumentKit
//
//  Created by Zakk Hoyt on 3/1/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

public class InstructionTableViewCell: UITableViewCell {
    public static let identifier = "InstructionTableViewCell"
    @IBOutlet weak var disabledImageView: UIImageView!
    @IBOutlet weak var enabledImageView: UIImageView!
    @IBOutlet weak var legendTitleLabel: UILabel!
    @IBOutlet weak var legendAboutLabel: UILabel!

    override public func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    
}
