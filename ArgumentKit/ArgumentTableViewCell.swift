//
//  ArgumentTableViewCell.swift
//  ArgumentManagerExample
//
//  Created by Zakk Hoyt on 4/18/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import UIKit



public typealias ArgumentTableViewCellButtonTapped = () -> Void

public class ArgumentTableViewCell: UITableViewCell {

    public static let identifier = "ArgumentTableViewCell"
    
    public var argumentPresenceItem: ArgumentPresenceItem? {
        didSet {
            updateUI()
        }
    }
    
    public var xcodeButtonTapped: ArgumentTableViewCellButtonTapped?
    public var userDefaultsButtonTapped: ArgumentTableViewCellButtonTapped?
    public var runtimeButtonTapped: ArgumentTableViewCellButtonTapped?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var xcodeButton: UIButton!
    @IBOutlet weak var userDefaultsButton: UIButton!
    @IBOutlet weak var runtimeButton: UIButton!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        prepareForReuse()
        updateUI()
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = ""
        descriptionLabel.text = ""
    }
    
    private func updateUI() {
        guard let argument = self.argumentPresenceItem else {
            return
        }
        
        titleLabel.text = argument.title
        descriptionLabel.text = argument.description
        
        let bundle = Bundle(for: ArgumentTableViewCell.self)
        
        if argument.presence.contains(.xcode) {
            xcodeButton.setTitleColor(UIColor.green, for: .normal)
            let image = UIImage(named: "xcode_filled_30x30", in: bundle, compatibleWith: nil)
            xcodeButton.setImage(image, for: .normal)
        } else {
            xcodeButton.setTitleColor(UIColor.darkGray, for: .normal)
            let image = UIImage(named: "xcode_30x30", in: bundle, compatibleWith: nil)
            xcodeButton.setImage(image, for: .normal)
        }
        
        if argument.presence.contains(.userDefaults) {
            userDefaultsButton.setTitleColor(UIColor.green, for: .normal)
            let image = UIImage(named: "userDefaults_filled_30x30", in: bundle, compatibleWith: nil)
            userDefaultsButton.setImage(image, for: .normal)
        } else {
            userDefaultsButton.setTitleColor(UIColor.darkGray, for: .normal)
            let image = UIImage(named: "userDefaults_30x30", in: bundle, compatibleWith: nil)
            userDefaultsButton.setImage(image, for: .normal)
        }
        
        if argument.presence.contains(.runtime) {
            runtimeButton.setTitleColor(UIColor.green, for: .normal)
            let image = UIImage(named: "runtime_filled_30x30", in: bundle, compatibleWith: nil)
            runtimeButton.setImage(image, for: .normal)
        } else {
            runtimeButton.setTitleColor(UIColor.darkGray, for: .normal)
            let image = UIImage(named: "runtime_30x30", in: bundle, compatibleWith: nil)
            runtimeButton.setImage(image, for: .normal)
        }
    }
    
    @IBAction func xcodeButtonTouchUpInside(_ sender: Any) {
        xcodeButtonTapped?()
    }
    
    @IBAction func userDefaultsButtonTouchUpInside(_ sender: Any) {
        userDefaultsButtonTapped?()
    }
    
    @IBAction func runtimeButtonTouchUpInside(_ sender: Any) {
        runtimeButtonTapped?()
    }
    
}
