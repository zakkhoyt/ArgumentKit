### ArgumentManager

A visual way to manage arguments to your iOS application. At a glance, see which arguments have been passed to your app by Xcode. Use/persists those same arguments by storing them in UserDefaults, or for the current run-time.

### Description

When developing an app, it is common to add arguments to Xcode which modifies the behavior of your app

![](http://i.imgur.com/G3bzoUv.png)


ArgumentManager makes it easy to extend these arguments to your QA users. The developer can still pass arguments in via Xcode scheme as normal. The included table view allows your users to toggle arguments on and off in both non-volatile and volatile fashions.

![](http://i.imgur.com/dyXFzPu.png)

##### Legend

Xcode - Argument is enabled/disabled by Xcode by adding an argument under Scheme ▻ Run ▻ Arguments. These arguments are only active if Xcode's debugger launched the app. App has read-only permissions.

UserDefaults - Argument is enabled/disabled by the user and is stored in UserDefaults. Sticky across app sessions (non-volatile). Arguments continue to function outside of the debug environment. App has read-write permissions.

Runtime - Argument is enabled/disabled by the user and is stored in RAM. State is cleared every app session (volatile). Arguments continue to function outside of the debug environment. App has read-write permissions.

##### Apply Arguments

Check if these argument are enabled to modify your code flow:

````
if ArgumentManager.shared.argumentEnabled(Argument.darkTheme.rawValue) {
    // set up dark theme
} else {
    // set up light theme
}
````

### Usage

1.) Installation
Manual: Clone this repository and then drag ArgumentManager.xcodeproj in to your project
Cocoapods: pod ArgumentKit // TODO
Carthage: // TODO

2.) Define your arguments

````
import ArgumentKit

enum Arguments: String {
    case darkTheme = "DARK_THEME"
}
````

3.) Call the setup function on the above class from your AppDelegate or other appropriate location:

````
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

    ArgumentManager.shared.arguments = [
        Argument(title: Arguments.darkTheme.rawValue, description: "Render the UI in a darker theme"),
    ]

    return true
}
````


* You can expose the included UITableViewController (shown above) using typical navigationController, tabController, or modal techniques.
* Your users can toggle arguments on/off by tapping the UD (UserDefaults) or RT (Runtime) buttons.
* Developers can set Xcode arguments by adding them to the target's scheme (shown above). Tapping on Xc button does nothing.

````
@IBAction func settingsBarbAction(_ sender: Any) {
    let argumentsViewController = ArgumentManagerTableViewController.instance()
    navigationController?.pushViewController(argumentsViewController, animated: true)
}
````

* Check for arguments to modify your code flow:

````
if ArgumentManager.shared.argumentEnabled(Argument.darkMode.rawValue) {
    // Configure your view for DarkMode
} else {
    // Configure your view for LightMode
}
````

* Changes may be observed via Notifications

````
NotificationCenter.default.addObserver(forName: .ArgumentManagerArgumentsDidChange,
                                        object: nil,
                                         queue: .main) { (note) in
                                         // update your code here
}
````

### Example

Clone this repository and open ArgumentManagerExample.xcodeproj
